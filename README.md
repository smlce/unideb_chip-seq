# Unideb_ChIP-seq

ChIP-seq pipeline for the BBL group at UD.

## Introduction

This is an ChIP-seq pipeline which expects to start from Illumina sequenced raw fastq files, and it will perform the trimming, QC, alignment and peak calling. Currently it only supports single reads. The peak calling can be with or without a control sample.

## Installation and setup

This a single shell script pipeline. Installation is as easy as cloning the the repository with git to your local computer. Check if you have privileges to write and to run the script. Open the script, and change the values of the "install_dir" and "index_dir" variables; these are the only two values you need to preset according to your local settings. "install_dir" is the directory where this script (and any linked scripts) are located; "index_dir" is the directory where the indexes for alignment, counting etc. are located. All the other values are set on the command line when you start the script.

However, the script has some prerequisites before it is ready to run, please check the next chapter.

### Prerequisites

To properly run the script, you need to have certain software tools and indexes ready on your computer. The software tools should be on your PATH so that the script can call them wherever hey are actually installed. The indexes are need to be in the "index_dir" directory.

### Software tools
The following tools are necessary to run the script (in brackets you can find the version this pipeline was tested with; newer versions should be backward compatible):

- FastQC (0.11.9)
- cutadapt (3.4)
- TrimGalore! (0.6.7)
- BWA (0.7.17)
- MACS2 (2.2.7.1)

Please check the project pages and the manuals of the respective software tools for installation and setup instructions. Be aware that I am not maintaining these software, for troubleshooting please contact their respective support contacts.

### Indexes

The script is currently recommended to process hg19 samples, but other organisms and versions can be used too. Acquire the appropriate files from a reliable database, e.g. Ensembl. The following files are needed:

- BWA: genome file in multifasta format

Please follow the manuals of the respective software tools to prepare the genome indexes.

### Additional setup

Before you start the analysis, the project folder should already exist, and it should contain a directory named "raw_data". You should place your fastq files of the raw reads there. Everything else (including the rest of the folder structure) will be created automatically by the script. Exception: if you are using an already processed sample as a control (e.g. in your setup sever IP uses the same control, and so the control has been processed already for a previous peak calling), then the control alignment should be in the "aligned" subfolder within the project directory.

## Running the script

Once everything is set up properly, you can run the script as any shell script (it is written in bash shell).

`sh M_ChIP-seq.sh <sample_name> <raw_file> <control> <genome> <project>`

The command line parameters of the script are the following:

**sample_name** is the unique name for your sample that you want to use throughout the analysis for various outputs

**raw_file** is the name of the raw sequencing file you downloaded

**control** can be 3 strings with different meanings: either set it to 0 if you want to call peaks without a control sample; or give the full name of the raw sequencing file, similarly to the <raw_file> option for the IP sample (in this case the raw fastq file should be present in the "raw_data" folder); or if the control has been processed already (e.g. you are using the same control for multiple IPs), then give the sample name for the control alignment, and the program will look for a .bam file in the /aligned folder (you should copy or link the control alignment there before starting the analysis)

**genome** is the name and version of the organism and genome version of your sample, it is used for references for alignment, peak calling, annotation etc. (currently recommended: hg19);

**project** is the name of the project, used for creating the project folder and generate the outputs in the subfolders of the project folder; in the project folder there should be a subfolder named "raw_data" that contains the gzipped raw fastq files (and in case you choose an already processed sample as a control, the "aligned" subfolder should contain the alignment, or at least a link to it).

An example of running the script:

`sh M_ChIP-seq.sh H3K27ac_5 SRR8134480.fastq.gz 0 hg19 ChIP-seq_project`

This would trigger the no-control pipeline where "H3K27ac_5" is the sample name, "SRR8134480.fastq.gz" is the raw read file of the IP sample in the "raw_data" subfolder, "0" means that the peaks should be called without a control sample, "hg19" is the genome version, and "ChIP-seq_project" is the project name.
The script performs the following steps:

1. Creating the folder structure
2. Creating links to your raw read files with the appropriate sample names (the original files will be untouched)
3. Quality check with FastQC
4. Trimming the standard Illumina adapters with TrimGalore/cutadapt
5. Post-trimming QC with FastQC
6. Aligning the reads to the genome with BWA (outputs sam files by default, samtools is used to sort and convert sam files to bam files)
7. Peak calling with or without a control sample


## Output and downstream analyses

The pipeline will create the following outputs from the aforementioned steps: trimmed read files, alignment files, peak files, and QC reports.

The most relevant for the downstream analyses are the alignment files (in bam format) and the peak files (in bed format). With these information, knowing the positions of the peaks, their read counts and their enrichment level compared to the background, you are able to compare different sample groups, e.g. the ChIP-seq profiles of healthy and diseased samples. These comparisons can range from finding overlapping peak positions (e.g. to determine consensus peaks in a sample group), through visualizations, like hierarchical clustering and heatmaps, to searching for differentially enriched peaks between sample groups with refined statistical tests. Several tools are available for these tasks, like bedtools for manipulating peak files, various R packages (e.g. ggplot2) for visualization and tests, DiffBind for identifying differentially enriched peaks.  
Motif search can be performed to identify common motifs in the enriched regions, these are often typical for a DNA binding protein. Homer is a well-known software suit for ChIP-seq data analysis, motif finding is one of its many functionalities.  
The peaks (either the unfiltered peaks, or the differential peaks between sample groups) can be annotated to see which genes or other genomic features they might belong to, and based on that a gene ontology/pathway analysis can be conducted to identify the crucial signalling pathways, cell components, interactions etc. that are relevant for the research project, e.g. affected by a disease in a diseased-healthy ChIP-seq comparison. Homer can also do the annotation and gene ontology, and there are online tools as well for finding relevant pathways to your identified genes and proteins, e.g. Reactome, DAVID and STRING.

There are countless possibilities for downstream analyses, to get an overview of the full ChIP-seq data analysis process and get inspired, please have a look at the SequencEnG website.

## Troubleshooting

For any bugs found feel free to create an issue in this project page.
