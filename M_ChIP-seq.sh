#!/bin/sh


## Master script for running ChIP-seq analysis (trimming - alignment - peak calling - QC). It is currently available only for SE sequencing.
# Run it as:
# sh M_ChIP-seq.sh <sample_name> <raw_file> <control> <genome> <project>
# where
# <sample_name> is the unique name for your sample that you want to use throughout the analysis for various outputs;
# <raw_file> is the name of the raw sequencing file you downloaded;
# <control> can be 3 strings with different meanings: either set it to 0 if you want to call peaks without a control sample; or give the full name of the raw sequencing file, similarly to the <raw_file> option for the IP sample (in this case the raw fastq file should be present in the "raw_data" folder); or if the control has been processed already (e.g. you are using the same control for multiple IPs), then give the sample name for the control alignment, and the program will look for a <control>.bam file in the <project>/aligned folder (you should copy or link the control alignment there before starting the analysis);
# <genome> is the name and version of the organism and genome version of your sample, it is used for references for alignment, peak calling, annotation etc. (currently available: hg19);
# <project> is the full path of the project where the subfolders and output files will be created; the project folder should already exist and there should be a subfolder named "raw_data" that contains the gzipped raw fastq files.
# Example: sh M_ChIP-seq.sh H3K27ac_5 SRR8134480.fastq.gz 0 hg19 /data/workgroup/ChIP-seq_project


## Set these variables manually:
# install_dir to the directory where this script (and any linked scripts) are located;
# index_dir to the directory where the indexes for alignment, counting etc. are located.

install_dir="/data10/working_groups/balint_group/m/programs/M_ChIPseq"
index_dir="/data10/working_groups/balint_group/m/index"


## Start of the analysis.

date -R
echo "ChIP-seq analysis script started."


## Reading command line arguments, setting up variables for the scripts.

sample_name=$1
raw_file=$2
control=$3
genome=$4
project_dir=$5

if [ ${control} == 0 ]
then
echo "Peaks will be called without a control."
else
echo "Peaks will be called with the sample ${control} as control."
fi

if [ ${control} == 0 ]
then
mode="nocontrol"
elif [ -e ${project_dir}/raw_data/${control} ]
then
mode="newcontrol"
elif [ -e ${project_dir}/aligned/${control}.bam ]
then
mode="oldcontrol"
else
mode="nocontrol"
echo "${control} is not recognized, control sample will not be processed."
fi

case ${genome} in
	"hg19")
	macs_gensize="hs"
	;;
	"hg38")
	macs_gensize="hs"
	;;
esac


## Creating the folder structure.

cd ${project_dir}
mkdir ${project_dir}/qc
mkdir ${project_dir}/trimmed
mkdir ${project_dir}/aligned
mkdir ${project_dir}/peaks


## Creating links for raw files (original files are left untouched).

cd ${project_dir}/raw_data
case ${mode} in
	"nocontrol")
	ln -s -f -T ${raw_file} ${sample_name}.fq.gz
	;;
	"newcontrol")
	ln -s -f -T ${raw_file} ${sample_name}.fq.gz
	ln -s -f -T ${control} ${sample_name}_control.fq.gz
	;;
	"oldcontrol")
	ln -s -f -T ${raw_file} ${sample_name}.fq.gz
	;;
esac


## Preliminary QC.

cd ${project_dir}/qc
case ${mode} in
	"nocontrol")
	fastqc --noextract --nogroup -o ./ ${project_dir}/raw_data/${sample_name}.fq.gz
	;;
	"newcontrol")
	fastqc --noextract --nogroup -o ./ ${project_dir}/raw_data/${sample_name}.fq.gz
	fastqc --noextract --nogroup -o ./ ${project_dir}/raw_data/${sample_name}_control.fq.gz
	;;
	"oldcontrol")
	fastqc --noextract --nogroup -o ./ ${project_dir}/raw_data/${sample_name}.fq.gz
	;;
esac


## Trimming (standard Illumina adapters) and QC.

cd ${project_dir}/trimmed
case ${mode} in
	"nocontrol")
	trim_galore --fastqc --length 15 ${project_dir}/raw_data/${sample_name}.fq.gz
	;;
	"newcontrol")
	trim_galore --fastqc --length 15 ${project_dir}/raw_data/${sample_name}.fq.gz
	trim_galore --fastqc --length 15 ${project_dir}/raw_data/${sample_name}_control.fq.gz
	;;
	"oldcontrol")
	trim_galore --fastqc --length 15 ${project_dir}/raw_data/${sample_name}.fq.gz
	;;
esac


## Alignment.

cd ${project_dir}/aligned
case ${mode} in
	"nocontrol")
	bwa mem ${index_dir}/bwa/genome/${genome}/${genome} ${project_dir}/trimmed/${sample_name}_trimmed.fq.gz > ${sample_name}.sam
	samtools sort -O bam -o ${sample_name}.bam ${sample_name}.sam
	rm ${sample_name}.sam
	;;
	"newcontrol")
	bwa mem ${index_dir}/bwa/genome/${genome}/${genome} ${project_dir}/trimmed/${sample_name}_trimmed.fq.gz > ${sample_name}.sam
	bwa mem ${index_dir}/bwa/genome/${genome}/${genome} ${project_dir}/trimmed/${sample_name}_control_trimmed.fq.gz > ${sample_name}_control.sam
	samtools sort -O bam -o ${sample_name}.bam ${sample_name}.sam
	samtools sort -O bam -o ${sample_name}_control.bam ${sample_name}_control.sam
	rm ${sample_name}.sam
	rm ${sample_name}_control.sam
	;;
	"oldcontrol")
	bwa mem ${index_dir}/bwa/genome/${genome}/${genome} ${project_dir}/trimmed/${sample_name}_trimmed.fq.gz > ${sample_name}.sam
	samtools sort -O bam -o ${sample_name}.bam ${sample_name}.sam
	rm ${sample_name}.sam
	;;
esac


## Peak calling.

cd ${project_dir}/peaks
case ${mode} in
	"nocontrol")
	macs2 callpeak -t ${project_dir}/aligned/${sample_name}.bam -f BAM -q 0.05 -n ${sample_name} -g ${macs_gensize} --keep-dup 1
	;;
	"newcontrol")
	macs2 callpeak -t ${project_dir}/aligned/${sample_name}.bam -c ${project_dir}/aligned/${sample_name}_control.bam -f BAM -q 0.05 -n ${sample_name} -g ${macs_gensize} --keep-dup 1
	;;
	"oldcontrol")
	macs2 callpeak -t ${project_dir}/aligned/${sample_name}.bam -c ${project_dir}/aligned/${control}.bam -f BAM -q 0.05 -n ${sample_name} -g ${macs_gensize} --keep-dup 1
	;;
esac


## End of the analysis.

echo "ChIP-seq analysis script finished."
date -R

